package Task15;

import common.StudentInGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static task11.Streams.getStream;

public class StreamLast0 {

    public static long time;

    public static Optional<String> randomGroup() {
        String[] groups = {"11-801", "11-802", "11-803", "11-804", "11-805", "11-806", "11-807", "11-808", "11-809", "11-810"};
        int a = (int) (10 * Math.random());
        return Optional.of(groups[a]);
    }

    public StudentInGroup distribution(StudentInGroup student) {
        long before = System.nanoTime();
        if (student.getGroupNumber().equals(Optional.empty())) {
            student.setGroupNumber(randomGroup());
            long after = System.nanoTime();

            time += after - before;
        }
        return student;
    }

    public void streamlast() {

        Supplier<Stream<StudentInGroup>> students0 = () -> getStream(20);

        ArrayList<StudentInGroup> studentsCollect = (ArrayList<StudentInGroup>) students0.get().collect(Collectors.toList());

        Supplier<Stream<StudentInGroup>> students = studentsCollect::stream;

        Stream<StudentInGroup> toChange = students.get();

        System.out.println("Время работы: " + time + " наносекунд.");

        System.out.println(Arrays.toString(studentsCollect.toArray()));

        ArrayList<StudentInGroup> changed = (ArrayList<StudentInGroup>) toChange.peek((s) -> distribution(s)).collect(Collectors.toList());

        System.out.println(changed);

    }


}
