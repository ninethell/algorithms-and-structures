package Task24;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    static class FibThread implements Runnable {
        long workingTime;

        @Override
        public void run() {
            countTime(10);
        }

        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                return fib(n - 2) + fib(n - 1);
            } else {
                return 1;
            }
        }

        private void countTime(int n) {
            long start = System.nanoTime();
            fib(n);
            System.out.println("Поток " + Thread.currentThread().getName() + " Закончил вычисления");
            workingTime = System.nanoTime() - start;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        ArrayList<FibThread> threads = new ArrayList<>();
        for(int i = 0; i < 10; i++) threads.add(new FibThread());

        //1
        ExecutorService s1 = Executors.newCachedThreadPool();

        long before = System.nanoTime();
        for(int i = 0; i < 10; i++) s1.submit(threads.get(i));

        s1.shutdown();
        s1.awaitTermination(100, TimeUnit.MINUTES);
        long after = System.nanoTime();
        System.out.println("Cached thread pool: " + (after - before));

        //2
        ExecutorService s2 = Executors.newFixedThreadPool(10);

        before = System.nanoTime();
        for(int i = 0; i < 10; i++) s2.submit(threads.get(i));

        s2.shutdown();
        s2.awaitTermination(100, TimeUnit.MINUTES);
        after = System.nanoTime();
        System.out.println("Fixed thread pool: " + (after - before));

        //3
        ExecutorService s3 = Executors.newFixedThreadPool(10);

        before = System.nanoTime();
        for(int i = 0; i < 10; i++) s3.submit(threads.get(i));

        s3.shutdown();
        s3.awaitTermination(100, TimeUnit.MINUTES);
        after = System.nanoTime();
        System.out.println("Single thread pool: " + (after - before));
    }


}
