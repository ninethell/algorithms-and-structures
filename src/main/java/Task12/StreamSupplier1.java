package Task12;

import common.StudentInGroup;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import task11.Streams;

public class StreamSupplier1 extends Streams{


    public static void getInfo() {

        long before = System.nanoTime();

        Supplier<Stream<StudentInGroup>> students = () -> getStreamParallel(10000);

        IntStream ages = students.get().parallel().mapToInt(StudentInGroup::getAge);

        Calendar calendar = new GregorianCalendar();

        int currentYear = calendar.get(Calendar.YEAR);

        IntStream years = ages.parallel().map(s -> currentYear - s);

        IntSummaryStatistics stat = years.summaryStatistics();

        long after = System.nanoTime();

        long time = after - before;

        System.out.println("\nСтатистика для #1: \nСреднее арифметическое: " + stat.getAverage()
                + "\nМаксимальный элемент: " + stat.getMax()
                + "\nМинимальный элемент: " + stat.getMin()
                + "\nСумма элементов: " + stat.getSum()
                + "\nКоличество элементов: " + stat.getCount()
                + "\nВремя работы: \n" + time + " наносек.\n");
    }
}
