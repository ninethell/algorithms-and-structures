package set.arraySet;

import set.Displayable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class OwnArraySet implements Set, Displayable {
    private int capacity;
    private Object[] array;

    public OwnArraySet(int max) {
        this.array = new Object[max];
        this.capacity = -1;
    }

    public int size() {
        return capacity + 1;
    }

    public boolean isEmpty() {
        if (capacity > -1) {
            return false;
        }
        return true;
    }

    public boolean contains(Object o) {
        for(int i = 0; i <= this.capacity; i++) {
            if (this.array[i].equals(o)) return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new Iterator() {

            int index = 0;

            @Override
            public boolean hasNext() {
                if (index <= capacity)
                {
                    return true;
                }
                else return false;
            }

            @Override
            public Object next() {
                return array[index++];
            }
        };
    }

    public Object[] toArray() {
        Object [] a = new Object [this.capacity+1];
        System.arraycopy(this.array, 0, a, 0, this.capacity);
        return a;
    }


    public boolean add(Object o) {
        if(this.capacity == -1) {
            this.capacity++;
            this.array[this.capacity] = o;
            return true;
        }

        for (int i = 0; i <= this.capacity; i++) {
            if (this.array[i].equals(o)) {
                return false;
            }
        }

        this.capacity++;
        this.array[this.capacity] = o;
        return true;
    }


    public boolean remove(Object o) {
        for (int i = 0; i <= this.capacity; i++){
            if (this.array[i].equals(o)){
                for (int j = i; j < this.capacity; j++){
                    this.array[j] = this.array[j+1];
                }
                this.capacity--;
                return true;
            }
        }
        return false;
    }

    public boolean addAll(Collection c) {
        boolean flag = false;

        for(Object i : c){
            for(int j = 0; j <= this.capacity; j++){
                if (i.equals(this.array[j])) flag = true;
            }
            if (!flag){
                this.capacity++;
                this.array[this.capacity] = i;
            }
        }
        return true;
    }

    public void clear() {
        this.capacity = -1;
    }

    public boolean removeAll(Collection c) {
        for(Object i : c){
            for(int j = 0; j <= this.capacity; j++){
                if (i.equals(this.array[j])) {
                    for (int k = j; k < this.capacity; k++){
                        this.array[k] = this.array[k+1];
                    }
                    this.capacity--;
                    break;
                }
            }
        }
        return true;
    }

    public boolean retainAll(Collection c) {
        for(Object i : c){
            for(int j = 0; j <= this.capacity; j++){
                if (!i.equals(this.array[j])) {
                    for (int k = j; k < this.capacity; k++){
                        this.array[k] = this.array[k+1];
                    }
                    this.capacity--;

                }
            }
        }
        return true;
    }

    public boolean containsAll(Collection c) {
        boolean flag = false;
        for (Object i : c){
            for (Object j : this.array){
                if(i.equals(j)) flag = true;
            }

            if (!flag) return false;
            flag = false;
        }
        return true;
    }

    public Object[] toArray(Object[] a) {
        a = new Object [this.capacity+1];
        System.arraycopy(this.array, 0, a, 0, this.capacity);
        return a;
    }

    public void display() {
        for (int i = 0; i <= capacity; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}