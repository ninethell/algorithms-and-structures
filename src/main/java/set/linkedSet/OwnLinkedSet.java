package set.linkedSet;

import set.Displayable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class OwnLinkedSet implements Set, Displayable {
    public Link first;

    public OwnLinkedSet(){
        this.first = null;
    }

    public int size() {
        return 0;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public boolean contains(Object o) {
        Link current = first;
        while(current.link != null){
            if (current.value.equals(o)) {
                return true;
            }
            current = current.link;
        }
        return false;
    }

    public Iterator iterator() {
        return new Iterator() {

            @Override
            public boolean hasNext() {
                if (first.link != null){
                    return true;
                }
                else return false;
            }

            @Override
            public Object next() {
                return first.link;
            }
        };
    }

    public Object[] toArray() {
        Link current = this.first;
        int index = 0;

        while (current.link != null) {
            index++;
            current = current.link;
        }

        current = this.first;

        Object [] a = new Object[index];

        for (int i = 0; i < index; i++){
            a[i] = current;
        }

        return a;
    }

    public boolean add(Object o) {
        if (this.first == null) {
            this.first = new Link(o);
            return true;
        }
        else {
            Link link = new Link(o);
            Link current = this.first;
            while (current.link != null){
                if (o.equals(current.value)) return false;
                current = current.link;
            }
            link.link = this.first;
            this.first = link;
            return true;
        }

    }

    public boolean remove(Object o) {
        Link current = first;
        while(current.link.link != null){
            if (current.link.value.equals(o)) {
                current.link = current.link.link;
                return true;
            }
            current = current.link;
        }
        return false;
    }

    public boolean addAll(Collection c) {
        for (Object i : c){
            Link link = new Link(i);
            if (this.first == null) this.first = link;
            else {
                Link current = first;
                boolean flag = false;
                while (current.link != null){
                    if (i.equals(current.value)) flag = true;
                    current = current.link;
                }
                if (i.equals(current.value)) flag = true;

                if(!flag) {
                    link.link = this.first;
                    this.first = link;
                }
            }
        }
        return true;
    }

    public void clear() {
        first = null;
    }

    public boolean removeAll(Collection c) {
        for (Object i : c){
            while (true) {
                if (this.first.value.equals(i)) {
                    if(this.first.link != null) this.first = this.first.link;
                    else return true;
                }
                else break;
            }

            if(this.first.link == null) return true;

            Link current = this.first;
            Link next = this.first.link;
            while(next.link != null){
                if (next.value.equals(i)) {
                    current.link = next.link;
                }
                current = current.link;
                next = next.link;
            }

        }
        return true;
    }

    public boolean retainAll(Collection c) {
        for (Object i : c){
            while (true) {
                if (!this.first.value.equals(i)) {
                    if(this.first.link != null) this.first = this.first.link;
                    else {
                        this.first = null;
                        return true;
                    }
                }
                else break;
            }

            if(this.first.link == null) return true;

            Link current = this.first;
            Link next = this.first.link;
            while(next.link != null){
                if (!next.value.equals(i)) {
                    current.link = next.link;
                }
                current = current.link;
                next = next.link;
            }

        }
        return true;
    }

    public boolean containsAll(Collection c) {
        for (Object i : c){
            Link current = first;
            boolean flag = false;
            while(current.link != null){
                if (current.value.equals(i)) {
                    flag = true;
                }
                current = current.link;
            }
            if (!flag) return false;
        }
        return true;
    }

    public Object[] toArray(Object[] a) {
        Link current = first;
        int i = 0;
        while(current.link != null){
            a[i] = current;
            current = current.link;
        }
        return a;
    }

    public void display() {
        if (this.first == null) return;
        Link current = this.first;
        while(current.link != null){
            System.out.print(current.value.toString() + " ");
            current = current.link;
        }
        System.out.println(current.value.toString());
    }
}