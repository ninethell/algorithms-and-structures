package set;

import set.linkedSet.OwnLinkedSet;

import java.util.ArrayList;
import java.util.Arrays;

public class OwnLinkedSetTester {
    public static void main(String[] args) {
        OwnLinkedSet ownLinkedSet = new OwnLinkedSet();

        ownLinkedSet.add(0);
        ownLinkedSet.add(11);
        ownLinkedSet.add(12);
        ownLinkedSet.add(3);

        ownLinkedSet.display();

        //ownLinkedSet.remove(new Integer(11));
        //ownLinkedSet.display();

        ArrayList<Integer> c = new ArrayList<>(Arrays.asList(10, 11, 12));
        ArrayList<Integer> d = new ArrayList<>(Arrays.asList(10, 15, 12));

        //ownLinkedSet.addAll(c);
        //ownLinkedSet.display();

ownLinkedSet.removeAll(c);
ownLinkedSet.display();

        ownLinkedSet.retainAll(c);
        ownLinkedSet.display();

        //ownLinkedSet.containsAll(c);
        //ownLinkedSet.containsAll(d);

    }
}
