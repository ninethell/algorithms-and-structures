package set;

public interface Displayable {
    public void display();
}