package set;
import set.arraySet.OwnArraySet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class OwnArraySetTester {
    public static void main(String[] args) {
        OwnArraySet ownArraySet = new OwnArraySet(100);
        System.out.println(ownArraySet.add(0));
        System.out.println(ownArraySet.add(1));
        System.out.println(ownArraySet.add(2));
        System.out.println(ownArraySet.add(3));
        System.out.println(ownArraySet.add(2));

        ArrayList<Integer> c = new ArrayList<>();
        c.add(11);
        c.add(12);
        c.add(13);
        ArrayList<Integer> d = new ArrayList<>(Arrays.asList(10, 11, 13));

        ownArraySet.display();

        ownArraySet.isEmpty();

        ownArraySet.contains(1);
        ownArraySet.contains(20);

        ownArraySet.remove(2);
        ownArraySet.display();

        ownArraySet.addAll(c);
        ownArraySet.display();

        ownArraySet.addAll(d);
        ownArraySet.display();

        ownArraySet.removeAll(c);
        ownArraySet.display();

        ownArraySet.retainAll(c);
        ownArraySet.display();

        ownArraySet.containsAll(c);

    }
}