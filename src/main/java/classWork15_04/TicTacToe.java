package classWork15_04;

import java.io.BufferedReader;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.io.File;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;


public class TicTacToe {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String filename = br.readLine();
        char[][] field = new char[3][3];
        boolean written = false;

        while (true) {
            try (RandomAccessFile file = new RandomAccessFile(new File(filename), "rw");
                 FileChannel fileChannel = file.getChannel();
                 FileLock lock = fileChannel.tryLock(0, 1, false)) {

                if (lock != null) {
                    written = false;
                    file.seek(0);
                    char symbol = file.readChar();
                    for (int i = 0; i < 9; i++) {
                        file.seek((i + 1) * 2);
                        char c = file.readChar();
                        int rowToInsert = i / 3;
                        int colToInsert = i % 3;
                        field[rowToInsert][colToInsert] = c;
                    }


                    if (checkWin(field) != symbol) {
                        if (checkWin(field) == '-' && !playable(field)) {
                            printField(field);
                            System.out.println("Ничья!");
                            break;
                        } else if (checkWin(field) != '-') {
                            printField(field);
                            System.out.println("Вы проиграли((");
                            break;
                        }
                    }

                    System.out.println("Ваш ход, вы ходите " + symbol + "ом, игровое поле: ");


                    printField(field);

                    System.out.print("Куда поставить " + symbol + "? ");
                    int answer = Integer.parseInt(br.readLine());
                    file.seek(answer * 2);
                    file.writeChar(symbol);

                    int rowToInsert = (answer - 1) / 3;
                    int colToInsert = (answer - 1) % 3;

                    field[rowToInsert][colToInsert] = symbol;

                    file.seek(0);
                    file.writeChar(symbol == 'X' ? '0' : 'X');

                    if (checkWin(field) == symbol) {
                        System.out.println("Вы выиграли!!!!!1");
                        break;
                    }

                } else {
                    if (!written) {
                        System.out.println("Дождитесьзавершения чужого хода(((");
                        written = true;
                    }
                }
            }
            Thread.sleep(1000);
        }
    }

    private static char checkWin(char[][] field) {
        boolean win = true;

        for (int i = 0; i < field.length; i++) {
            win = true;
            for (int j = 0; j < field.length - 1; j++) {
                if (field[i][j] != field[i][j + 1]) {
                    win = false;
                    break;
                }
            }
            if (win) {
                return field[i][0];
            }
        }

        for (int j = 0; j < field.length; j++) {
            win = true;
            for (int i = 0; i < field.length - 1; i++) {
                if (field[i][j] != field[i + 1][j]) {
                    win = false;
                    break;
                }
            }
            if (win) {
                return field[0][j];
            }
        }

        for (int i = 0; i < field.length - 1; i++) {
            win = true;
            if (field[i][i] != field[i + 1][i + 1]) {
                win = false;
                break;
            }
        }
        if (win) {
            return field[0][0];
        }

        for (int i = 0; i < field.length - 1; i++) {
            for (int j = field.length - 1; j > 0; j--) {
                win = true;
                if (field[i][j] != field[i + 1][j - 1]) {
                    win = false;
                    break;
                }
            }
        }
        if (win) {
            return field[field.length - 1][field.length - 1];
        }

        return '-';
    }

    private static boolean playable(char[][] field) {
        for (char[] aField : field) {
            for (int j = 0; j < field.length; j++) {
                if (aField[j] != '0' && aField[j] != 'X') {
                    return true;
                }
            }
        }
        return false;
    }

    private static void printField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
    }
}
