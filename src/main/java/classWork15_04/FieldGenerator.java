package classWork15_04;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class FieldGenerator{
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String filename = "field";

        RandomAccessFile raf = new RandomAccessFile(filename, "rw");
        for (int i = 0; i < 10; i++) {
            raf.seek(i * 2);
            if (i == 0) {
                raf.writeChar('X');
            } else {
                raf.writeChar(Character.forDigit(i, 10));
            }
        }
        raf.close();
    }
}