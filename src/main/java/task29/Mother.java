package task29;

public class Mother implements Runnable {
    @Override
    public void run() {
        synchronized (Place.friendOfSon) {
            System.out.println("Мама поймала сына с сигаретами");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Мама ждет пока сын бросит сигареты, иначе не вернёт ему телефон");
            synchronized (Place.cigarette) {
                System.out.println("Мама вернула телефон");
            }
        }
    }
}
