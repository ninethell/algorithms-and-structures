package task29;

public class Son implements Runnable {
    @Override
    public void run() {
        synchronized (Place.cigarette) {
            System.out.println("Сын соврал что курил его друг а он просто рядом стоял");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Сын ждет пока мама вернет телефон, иначе не выбросит сигареты");
            synchronized (Place.friendOfSon) {
                System.out.println("Сын выбросил сигареты");
            }
        }
    }
}