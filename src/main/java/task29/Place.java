package task29;

public class Place {
    static FriendOfSon friendOfSon = new FriendOfSon();
    static Cigarettes cigarette = new Cigarettes();

    //Две стороны ждут пока другая что-то сделает, и так застревает навсегда

    public static void main(String[] args) {
        new Thread(new Mother()).start();
        new Thread(new Son()).start();
    }
}