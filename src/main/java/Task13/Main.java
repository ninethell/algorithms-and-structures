package Task13;

public class Main {
    public static void main(String[] args) {

        long before;
        long after;
        long time;
        before = System.nanoTime();
        StreamMap0.streamMap0();
        after = System.nanoTime();

        time = after - before;

        System.out.println("Время работы на последовательных: " + time + " наносек.");


        before = System.nanoTime();
        StreamMap1.streamMap1();
        after = System.nanoTime();

        time = after - before;

        System.out.println("Время работы на параллельных: " + time + " наносек.");
    }
}
