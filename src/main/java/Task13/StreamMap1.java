package Task13;

import common.StudentInGroup;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static task11.Streams.*;

public class StreamMap1 {
    public static void streamMap1() {

        Supplier<Stream<StudentInGroup>> students = () -> getStream(10000);

        Map<String, List<StudentInGroup>> stringStudentInGroupMap0 = students.<StudentInGroup>get().parallel().collect(Collectors.groupingBy(StudentInGroup::getName));
        //System.out.println(stringStudentInGroupMap0);

        Map<String, List<StudentInGroup>> stringStudentInGroupMap1 = students.<StudentInGroup>get().parallel().collect(Collectors.groupingBy((s) -> s.getGroupNumber().orElse("none")));
        // System.out.println(stringStudentInGroupMap1);

        Map<String, List<String>> stringStudentInGroupMap2 = students.<StudentInGroup>get().parallel().collect(Collectors.toMap(StudentInGroup::getName, x -> {
                    List toReturn = new ArrayList<String>();
                    toReturn.add(x.getGroupNumber());
                    return toReturn;
                },
                (x, y) -> {
                    x.addAll(y);
                    return x;
                }));
        //System.out.println(stringStudentInGroupMap2);

        Map<String, List<String>> stringStudentInGroupMap3 = students.<StudentInGroup>get().parallel().collect(Collectors.toMap((s) -> s.getGroupNumber().orElse("none"), x -> {
                    List toReturn = new ArrayList<String>();
                    toReturn.add(x.getName());
                    return toReturn;
                },
                (x, y) -> {
                    x.addAll(y);
                    return x;
                }));
        //System.out.println(stringStudentInGroupMap3);

        Map<String, Map<Integer, List<StudentInGroup>>> stringStudentInGroupMap4 = students.<StudentInGroup>get().parallel().collect(Collectors.groupingBy((s) -> s.getGroupNumber().orElse("none"),Collectors.groupingBy(StudentInGroup::getAge)));
        //System.out.println(stringStudentInGroupMap4);


    }
}
