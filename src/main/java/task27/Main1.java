package task27;

public class Main1 implements Runnable {

    IntGenerator intGenerator;
    private Object syncPoint;

    Main1(IntGenerator intGenerator) {
        this.intGenerator = intGenerator;
    }

    /**
     * При синхронизации по разным потокам получаем (и очень быстро) вход в генератор двух различных потоков, из-за
     * чего value в какой-то момент принимает нечетное значение и происходит выход из генератора. Такой подход, очевидно,
     * не является потокобезопасным
     *
     * @param args
     */
    public static void main(String[] args) {
        IntGenerator intGenerator = new IntGeneratorSync();
        Main1 main1 = new Main1(intGenerator);
        Main1 main2 = new Main1(intGenerator);
        Main1 main3 = new Main1(intGenerator);
        Main1 main4 = new Main1(intGenerator);
        Object sync1 = new Object();
        Object sync2 = new Object();
        main1.setSyncPoint(sync1);
        main2.setSyncPoint(sync1);
        main3.setSyncPoint(sync2);
        main4.setSyncPoint(sync2);
        Thread thread1 = new Thread(main1);
        Thread thread2 = new Thread(main2);
        Thread thread3 = new Thread(main3);
        Thread thread4 = new Thread(main4);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }

    public void setSyncPoint(Object syncPoint) {
        this.syncPoint = syncPoint;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            i++;
            int value = intGenerator.getNext(syncPoint);
            if (value % 2 != 0) {
                System.out.println(value + " " + value % 2);
                System.exit(-1);
            }
            if (i % 1000 == 0) {
                System.out.println(value);
            }
        }
    }
}
