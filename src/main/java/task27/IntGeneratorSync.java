package task27;

public class IntGeneratorSync implements IntGenerator {
    int value = 0;

    @Override
    public int getNext(Object syncPoint) {
        synchronized (syncPoint) {
            value++;
            value++;
            return value;
        }
    }
}