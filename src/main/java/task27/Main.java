package task27;

public class Main implements Runnable {

    IntGenerator intGenerator;
    private Object syncPoint;

    Main(IntGenerator intGenerator) {
        this.intGenerator = intGenerator;
    }

    public static void main(String[] args) {
        IntGenerator intGenerator = new IntGeneratorSync();
        Main main1 = new Main(intGenerator);
        Main main2 = new Main(intGenerator);
        Main main3 = new Main(intGenerator);
        Main main4 = new Main(intGenerator);
        Object sync1 = new Object();
        main1.setSyncPoint(sync1);
        main2.setSyncPoint(sync1);
        main3.setSyncPoint(sync1);
        main4.setSyncPoint(sync1);
        Thread thread1 = new Thread(main1);
        Thread thread2 = new Thread(main2);
        Thread thread3 = new Thread(main3);
        Thread thread4 = new Thread(main4);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }

    public void setSyncPoint(Object syncPoint) {
        this.syncPoint = syncPoint;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            i++;
            int value = intGenerator.getNext(syncPoint);
            if (value % 2 != 0) {
                System.out.println(value + " " + value % 2);
                System.exit(-1);
            }
            if (i % 1000 == 0) {
                System.out.println(value);
            }
        }
    }
}