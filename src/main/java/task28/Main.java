package task28;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class Main {
    static PipedReader reader = new PipedReader();
    static PipedWriter writer = new PipedWriter();

    public static void main(String[] args) throws IOException {
        reader.connect(writer);
        new Thread(new FibThread()).start();
        new Thread(new FibReader()).start();
    }

    static class FibThread implements Runnable {
        @Override
        public void run() {
            Fib(10);
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private int Fib(int n) {
            try {

                if (n == 0 || n == 1) {
                    writer.write(1);
                    return 1;
                } else {
                    int res = Fib(n - 2) + Fib(n - 1);
                    writer.write(res);
                    return res;
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
    }

    static class FibReader implements Runnable {
        @Override
        public void run() {
            try {
                while (reader.ready()) {
                    System.out.println(reader.read());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}