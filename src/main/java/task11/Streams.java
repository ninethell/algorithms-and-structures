package task11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import common.Student;
import common.StudentInGroup;

public class Streams {



    public static String randomName() {
        String[] names = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        int a = (int) (10 * Math.random());
        return names[a];
    }


    public static Optional<String> randomGroup() {
        String[] groups = { "11-801", "11-802", "11-803", "11-804", "11-805", "11-806", "11-807", "11-808", "11-809", "11-810"};
        int a = (int) (10*Math.random());
        int b = (int) (10*Math.random());
        if (b < 3) return Optional.empty();
        return Optional.of(groups[a]);
    }

    public static int randomAge() {
        int a = (int) (10 * Math.random());
        if (a <= 1) return 16;
        if (a <= 3) return 17;
        if (a <= 5) return 18;
        if (a <= 7) return 19;
        else return 20;
    }

    public static boolean randomIsGirl(){
        int a = (int)(10*Math.random());
        return a <= 5;
    }

    public static Stream<StudentInGroup> getStream(int limit){
        Comparator<StudentInGroup> comparator = new Comparator<StudentInGroup>(){
            public int compare(StudentInGroup o1, StudentInGroup o2) {
                return o1.compareTo(o2);
            }
        };

        return Stream.generate(() -> new StudentInGroup(randomName(), randomGroup(), randomAge(), randomIsGirl()))
                .distinct()
                .limit(limit)
                .sorted(comparator);
    }

    public static Stream<StudentInGroup> getStreamParallel(int limit){
        Comparator<StudentInGroup> comparator = new Comparator<StudentInGroup>(){
            public int compare(StudentInGroup o1, StudentInGroup o2) {
                return o1.compareTo(o2);
            }
        };

        return Stream.generate(() -> new StudentInGroup(randomName(), randomGroup(), randomAge(), randomIsGirl()))
                .parallel()
                .distinct()
                .limit(limit)
                .sorted(comparator);
    }

    public static void main(String[] args) {

        List<StudentInGroup> a = getStream(20).collect(Collectors.toList());
        System.out.println(Arrays.toString(a.toArray()));

    }
}
