package task11;

import java.util.Optional;

/**
 * Created by eljah32 on 3/12/2019.
 */
public class StudentInGroup implements Comparable{
    static {
        System.out.println("common.StudentInGroup loaded");
    }

    private Optional<String> groupNumber;
    private String name;
    private boolean isGirl;
    private int age;

    @Override
    public String toString() {
        return "common.StudentInGroup{" +
                "groupNumber=" + groupNumber +
                ", name='" + name + '\'' +
                ", isGirl=" + isGirl +
                ", age=" + age +
                '}';
    }

    public StudentInGroup(String name, Optional<String> groupNumber, int age, boolean isGirl)
    {
        this.name = name;
        this.groupNumber = groupNumber;
        this.age = age;
        this.isGirl = isGirl;
    }

    public Optional<String> getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Optional<String> groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGirl() {
        return isGirl;
    }

    public void setGirl(boolean girl) {
        isGirl = girl;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public int compareTo(Object o) {
        return this.getName().compareTo(((StudentInGroup) o).getName());
    }
}
