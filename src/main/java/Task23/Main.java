package Task23;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Main {

    static class Fib extends Thread {
        long workingTime;

        @Override
        public void run() {
            countTime(13);
            Thread.yield();
        }


        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                return fib(n - 2) + fib(n - 1);
            } else {
                return 1;
            }
        }

        private void countTime(int n) {
            long start = System.nanoTime();
            fib(n);
            workingTime = System.nanoTime() - start;
        }
    }

    static class FibWithoutYield extends Thread {
        long workingTime;

        @Override
        public void run() {
            countTime(10);
        }

        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                return fib(n - 2) + fib(n - 1);
            } else {
                return 1;
            }
        }

        private void countTime(int n) {
            long start = System.nanoTime();
            fib(n);
            workingTime = System.nanoTime() - start;
        }
    }

    public static void main(String[] args) {

        //1
        System.out.println("Максимальный приоритет, демон и yield " + Stream.generate(() -> {
            ArrayList<Fib> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new Fib());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(10);
                threads.get(i).setDaemon(true);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        //2
        System.out.println("Максимальный приоритет, демон и без yield " + Stream.generate(() -> {
            ArrayList<FibWithoutYield> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new FibWithoutYield());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(10);
                threads.get(i).setDaemon(true);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        //3
        System.out.println("Максимальный приоритет, не демон и yield " + Stream.generate(() -> {
            ArrayList<Fib> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new Fib());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(10);
                threads.get(i).setDaemon(false);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        //4
        System.out.println("Максимальный приоритет, не демон и без yield " + Stream.generate(() -> {
            ArrayList<FibWithoutYield> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new FibWithoutYield());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(10);
                threads.get(i).setDaemon(false);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        //5
        System.out.println("Минимальный приоритет, демон и yield " + Stream.generate(() -> {
            ArrayList<Fib> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new Fib());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(1);
                threads.get(i).setDaemon(true);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());


        //6
        System.out.println("Минимальный приоритет, демон и без yield " + Stream.generate(() -> {
            ArrayList<FibWithoutYield> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new FibWithoutYield());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(1);
                threads.get(i).setDaemon(true);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());


        //7
        System.out.println("Минимальный приоритет, не демон и yield " + Stream.generate(() -> {
            ArrayList<Fib> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new Fib());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(1);
                threads.get(i).setDaemon(false);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        //8
        System.out.println("Минимальный приоритет, не демон и без yield " + Stream.generate(() -> {
            ArrayList<FibWithoutYield> threads = new ArrayList<>();

            for (int i = 0; i < 4; i++) threads.add(new FibWithoutYield());

            for (int i = 0; i < 4; i++) {
                threads.get(i).setPriority(1);
                threads.get(i).setDaemon(false);
            }

            long start = System.nanoTime();
            for (int i = 0; i < 4; i++) threads.get(i).start();

            try {
                for (int i = 0; i < 4; i++) threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - start;
        }).limit(100).mapToLong(e -> e).summaryStatistics());
    }
}
