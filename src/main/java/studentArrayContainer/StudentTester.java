package studentArrayContainer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StudentTester {
    public static void main(String[] args) throws ParseException {

        Student student1 = new Student("a1", "b1", "c1");
        Student student2 = new Student("a2", "b2", "c2");
        Student student3 = new Student("a3", "b3", "c3");
        Student student4 = new Student("a4", "b4", "c4");
        student1.setAge(new SimpleDateFormat("dd.mm.yyyy").parse("20.01.1998"));//gettime 2
        student2.setAge(new SimpleDateFormat("dd.mm.yyyy").parse("08.01.1998"));//gettime 3
        student3.setAge(new SimpleDateFormat("dd.mm.yyyy").parse("13.11.1999"));//gettime 4
        student4.setAge(new SimpleDateFormat("dd.mm.yyyy").parse("25.12.2000"));//gettime 5

        StudentArrayContainer studentArrayContainer = new StudentArrayContainer(100);
        studentArrayContainer.add(student1);
        studentArrayContainer.add(student2);
        studentArrayContainer.add(student3);
        studentArrayContainer.add(student4);

        Student student5 = new Student("a5", "b5", "c5");
        student5.setAge(new SimpleDateFormat("dd.mm.yyyy").parse("24.12.1997"));//gettime 4.5
        System.out.println(student5.getAge().before(student1.getAge()));

        studentArrayContainer.insert(student5);
        studentArrayContainer.deleteStudent("a1", "b1", "c1");
        System.out.println(studentArrayContainer.toString());
    }
}
