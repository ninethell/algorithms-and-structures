package studentArrayContainer;

import java.util.Calendar;
import java.util.Date;

public class StudentArrayContainer {

    private Student[] array;
    private int capacity;

    public StudentArrayContainer(int max) {
        this.array = new Student[max];
        this.capacity = -1;
    }


    private void insert(int index, Student student){
        this.capacity++;
        for (int i = this.capacity; i > index; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[index] = student;
    }

    public void insert(Student student) {
        int L = 0;
        int R = this.capacity;
        boolean flag = false;

            if(student.getAge().getTime() < (this.array[0].getAge().getTime())){
                insert(0, student);
                flag = true;
            }

            if(student.getAge().getTime() > (this.array[this.capacity].getAge().getTime())){
                insert(this.capacity+1, student);
                flag = true;
            }

        if (!flag) {
            while (L != R) {
                int mid = L + (R - L) / 2;
                if (array[mid].getAge().getTime() > (student.getAge().getTime())) {
                    if (array[mid - 1].getAge().getTime() < (student.getAge().getTime())) {
                        insert(mid, student);
                        break;
                    } else R = mid;
                }
                if (array[mid].getAge().getTime() < (student.getAge().getTime())) {
                    if (array[mid + 1].getAge().getTime() > (student.getAge().getTime())) {
                        insert(mid + 1, student);
                        break;
                    } else L = mid;
                }
            }
        }
    }


    public void deleteStudent(String firstName, String lastName, String patronymic) {
        int i = 0;
        while (i <= this.capacity) {
            if (this.array[i].getFirstName().toLowerCase().equals(firstName.toLowerCase())) {
                if (this.array[i].getLastName().toLowerCase().equals(lastName.toLowerCase())) {
                    if (this.array[i].getPatronymic().toLowerCase().equals(patronymic.toLowerCase())) {
                        for (int j = i; j < this.capacity; j++){
                            this.array[j] = this.array[j+1];
                        }
                        this.capacity--;
                    }
                }
            }
            i++;
        }

    }


    public void add(Student student){
        this.capacity++;
        this.array[this.capacity] = student;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= this.capacity; i++) {
            stringBuilder.append("Фамилия: ");
            stringBuilder.append(this.array[i].getLastName());
            stringBuilder.append("\n");
            stringBuilder.append("Имя: ");
            stringBuilder.append(this.array[i].getFirstName());
            stringBuilder.append("\n");
            stringBuilder.append("Отчество: ");
            stringBuilder.append(this.array[i].getPatronymic());
            stringBuilder.append("\n");
            stringBuilder.append("Возраст: ");
            Calendar dateOfBirth = Calendar.getInstance();
            Calendar today = Calendar.getInstance();

            dateOfBirth.setTime(this.array[i].getAge());
            dateOfBirth.add(Calendar.DAY_OF_MONTH, -1);
            int yearsOld = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
            if (today.get(Calendar.DAY_OF_YEAR) <= dateOfBirth.get(Calendar.DAY_OF_YEAR)) yearsOld--;

            stringBuilder.append(yearsOld);
            stringBuilder.append("\n");
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
