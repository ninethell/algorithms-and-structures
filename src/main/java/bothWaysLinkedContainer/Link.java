package bothWaysLinkedContainer;

public class Link {

    public long value;
    public Link next;
    public Link previous;

    //Когда надо вставить вручную

    public Link(long value){

        this.value = value;
        this.next = null;
        this.previous = null;
    }
    //когда надо вставить в начало двухсвязного списка
    public Link(long value, Link next)
    {
        this.value = value;
        this.previous = null;
        this.next = next;
    }

    //когда надо вставить в какое-то место двухсвязного списка
    public Link(long value, Link next, Link previous){
        this.value = value;
        this.previous = previous;
        this.next = next;
    }

    public boolean hasNext()
    {
        return next != null;
    }

    public boolean hasPrevious()
    {
        return previous != null;
    }

    /*public void setLink(Link link) {
        this.link = link;
    }

    public Link getLink() {
        return link;
    }*/
}