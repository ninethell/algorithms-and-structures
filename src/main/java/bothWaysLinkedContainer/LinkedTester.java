package bothWaysLinkedContainer;

public class LinkedTester {
    public static void main(String[] args) {
        LinksContainer linksContainer=new LinksContainer();
        linksContainer.insert(10);
        linksContainer.insert(40);
        System.out.println(linksContainer.descending());
        linksContainer.insert(30);
        System.out.println(linksContainer.descending());
        linksContainer.insert(35);
        System.out.println(linksContainer.descending());
        linksContainer.insert(9);
        System.out.println(linksContainer.descending());
        linksContainer.remove(35);
        System.out.println(linksContainer.descending());
        System.out.println(linksContainer.ascending());


        /*SortedLinksContainer sortedLinksContainer = new SortedLinksContainer();
        sortedLinksContainer.insert(3);
        //sortedLinksContainer.insert(20);
        //sortedLinksContainer.insert(1);
        sortedLinksContainer.remove();

        System.out.println(sortedLinksContainer);
        */

    }
}