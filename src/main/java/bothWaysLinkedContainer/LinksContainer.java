package bothWaysLinkedContainer;


import common.Container;

public class LinksContainer implements Container {
    private Link first;
    private Link last;

    public LinksContainer(){

    }

    //добавляет в начало
    public void add(long value)
    {
        Link link = new Link(value, first);
        if (first != null){
            link.next = first;
            first.previous = link;
        }
        else this.first = link;
        if(this.last == null) this.last = this.first;
    }

    public void insert(long value) {
        boolean isAlone = false;
        if (this.first == null){
            Link mid = new Link(value);
            isAlone = true;
            this.first = mid;
            this.last = mid;
        }
            if ((this.first.next == null) && (this.first.previous == null)) {
                if (value > first.value) {
                    System.out.println(first.value);
                    System.out.println(last.value);
                    Link mid = new Link(value, first);
                    first.previous = mid;
                    isAlone = true;
                    this.first = mid;
                }
                if (value < first.value) {
                    Link mid = new Link(value, null, first);
                    first.next = mid;
                    isAlone = true;
                    this.last = mid;
                }
            }

        if (!isAlone){
        Link left = first;
        Link right = first.next;
            while (true) {
                System.out.println(first.value);
                System.out.println(last.value);
                if (value < left.value) {
                    if (right != null) {
                        if (value > right.value) {
                            Link mid = new Link(value, right, left);
                            left.next = mid;
                            right.previous = mid;
                            break;
                        }
                        left = right;
                        right = right.next;
                    } else {
                        Link mid = new Link(value);
                        mid.previous = left;
                        left.next = mid;
                        this.last = mid;
                        break;
                    }
                }
                else {
                    Link mid = new Link(value);
                    mid.next = last;
                    left.previous = mid;
                    break;
                }
            }
        }
    }

    public void remove(long value){
        Link current = this.first;

        while(true) {
            if (current != null) {
                if (current.value == value) {
                    if(current.previous != null) {
                        current.previous.next = current.next;
                        current.next.previous = current.previous;
                    }
                    else {
                        current.next.previous = null;
                        this.first = current.next;
                    }
                    break;
                }
            }
            if (current != null) current = current.next;
            else {
                System.out.println("Такого элемента нет, дурачок!!!");
                break;
            }
        }
    }


    public void display() {
        System.out.println(this.toString());
    }

    //по убыванию
    public String descending() {
        StringBuilder stringBuilder = new StringBuilder();
        Link current = this.first;
        if (current != null)
        {
            stringBuilder.append(current.value);
            stringBuilder.append(" ");
            while (current.hasNext())
            {
                stringBuilder.append(current.next.value);
                stringBuilder.append(" ");
                current = current.next;
            }
        }
        return stringBuilder.toString();
    }

    public String ascending(){
        StringBuilder stringBuilder = new StringBuilder();
        Link current = this.last;
        if (current != null)
        {
            stringBuilder.append(current.value);
            stringBuilder.append(" ");
            while (current.hasPrevious())
            {
                stringBuilder.append(current.previous.value);
                stringBuilder.append(" ");
                current = current.previous;
            }
        }
        return stringBuilder.toString();
    }

}