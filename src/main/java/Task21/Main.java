package Task21;


public class Main {
    static class Fib extends Thread {
        @Override
        public void run() {
            long start = System.nanoTime();
            fib(13);
            long time = (System.nanoTime() - start) / 1000000;
            System.out.println(Thread.currentThread().getName() + " вычислил за " + time + " миллисек.");
        }

        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                int num = fib(n - 2) + fib(n - 1);
                System.out.println(Thread.currentThread().getName() + ": " + num);
                return num;
            } else {
                System.out.println(Thread.currentThread().getName() + ": " + 1);
                return 1;
            }
        }
    }

    public static void main(String[] args) {
        for(int i = 0; i < 5; i++) new Fib().start();

    }
}
