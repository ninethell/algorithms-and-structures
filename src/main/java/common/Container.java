package common;

public interface Container {
    public void insert(long value);
    public void display();
}