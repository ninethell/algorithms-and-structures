package list;

import set.Displayable;

import java.util.*;

public class OwnLinkedList<E>  implements List<E>, Displayable {
    private Link<E> first;
    private Link<E> last;
    private int size;


    @Override
    public void display() {
        if (this.last == null) return;
        if (this.first == null) System.out.println(this.last.value.toString());

        Link current = this.last;
        while(current.hasPrevious()){
            System.out.print(current.value.toString() + " ");
            current = current.previous;
        }
        System.out.println(current.value.toString());
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return (this.first == null) && (this.last == null);
    }

    @Override
    public boolean contains(Object o) {
        if (this.size == 0) return false;
        if (this.size == 1) return this.last.value.equals((E)o);
        if (this.size == 2) return (this.last.value.equals((E)o)) || (this.first.value.equals((E)o));

            Link current = this.first;
            while (current.hasNext()) {
                if (current.value.equals((E)o)) return true;
                current = current.next;
            }

        return current.value.equals((E)o);
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object [] a = new Object[this.size];
        for(int i = 0; i < this.size; i++){
            a[i] = this.get(i);
        }
        return a;
    }

    @Override
    public boolean add(Object o) {
        if (this.last == null){
            this.last = new Link<>((E)o);
            this.last.previous = this.first;
            this.size++;
            return true;
        }
        if (this.first == null){
            this.first = new Link<>((E)o);
            this.first.next = this.last;
            this.last.previous = this.first;
            this.size++;
            return true;
        }
        else {
            Link<E> mid = new Link<>((E)o);
            mid.next = this.first;
            this.first.previous = mid;
            this.first = mid;
            this.size++;
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        boolean flag = false;

        if (this.size == 0) return false;
        if (this.size == 1) {
            if(this.last.value.equals((E)o)) {
                this.last = null;
                this.size--;
                return true;
            }
        }
        if (this.size == 2) {
            if (this.last.value.equals((E) o)) {
                this.last = null;
                this.size--;
                return true;
            }

            if (this.first.value.equals((E) o)) {
                this.first = null;
                this.size--;
                return true;
            }
        }

        if(this.first.value.equals(o)){
            this.first = this.first.next;
            this.first.previous = null;
            return true;
        }
            Link<E> current = this.first;
            while (current.hasNext()) {
                if (current.value.equals(o)) {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                    this.size--;
                    return true;
                }
                current = current.next;
            }
            if (current.value.equals(o)) {
                current.previous.next = current.next;
                current.next.previous = current.previous;
                this.size--;
                return true;
            }

        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {

        if(this.size == 0) {
            for (E e : c) this.add(e);
        }
        else{
            for (E e : c){
                Link<E> link = new Link<>(e);
                link.next = this.first;
                this.first.previous = link;
                this.first = link;
            }
        }
        this.size += c.toArray().length;
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if(this.size < index){
            return false;
        }

        if(this.size == index){
            for (E e : c){
                Link<E> link = new Link<>(e);
                this.first.previous = link;
                link.next = this.first;
                this.first = link;
            }
            this.size += c.toArray().length;
            return true;
        }

        Link<E> left = this.first;
        Link<E> right = left.next;
        for(int i = 0; i < index; i++){
            left = left.next;
            right = right.next;
        }

        for(E e : c){
            Link<E> link = new Link<>(e);
            left.next = link;
            link.previous = left;
        }
        left.next.next = right;
        right.previous = left.next;

        return true;
    }

    @Override
    public void clear() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    @Override
    public E get(int index) {
        Link<E> current = this.first;
        for (int i = 1; i < index; i++){
            current = current.next;
        }
        return current.value;
    }

    @Override
    public E set(int index, Object element) {
        Link<E> current = this.first;
        for (int i = 0; i < this.size; i++){
            current = current.next;
        }
        Link<E> a = new Link<>(current.value);
        current.value = (E)element;
        return a.value;
    }

    @Override
    public void add(int index, Object element) {
        if(this.size > index) {
            Link<E> link = new Link<>((E) element);
            Link<E> current = this.first;
            for (int i = 0; i < this.size; i++) {
                if (i == index) {
                    link.previous = current;
                    link.next = current.next;
                    current.next.previous = link;
                    current.next = link;
                    break;
                }
                current = current.next;
            }
        }
    }

    @Override
    public E remove(int index) {
        if(this.size < index) return null;
        if (index == 0){
            E e = this.last.value;
            this.last = this.last.previous;
            this.last.next = null;
            this.size--;
            return e;
        }
        Link<E> current = this.last;
        for(int i = 0; i < this.size; i++){
            if (i == index) {
                current.previous.next = current.next;
                current.next.previous = current.previous;
                break;
            }
            current = current.previous;
        }
        this.size--;
        return current.value;
    }

    @Override
    public int indexOf(Object o) {
        Link current = this.first;
        int i = 0;
        while (current.hasNext()){
            if(current.value.equals(o)) return i;
            current = current.next;
            i++;
        }
        if (current.value.equals(o)) return i;
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Link current = this.last;
        int i = this.size;
        while (current.hasPrevious()){
            if(current.value.equals(o)) return i;
            current = current.previous;
            i++;
        }
        if (current.value.equals(o)) return i;
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        List<E> a = new OwnLinkedList<>();
        for(int i = fromIndex; i < toIndex; i++){
            a.add(this.get(i));
        }
        return a;
    }

    @Override
    public boolean retainAll(Collection c) {
        Link<E> current = this.first;
        while (current.next != null) {
            boolean flag = false;
            for (Object o : c) {
                if (o.equals(current.value)) flag = true;
            }
            if (!flag){
                if (current.previous != null) {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                    this.size--;
                }
                else {
                    this.first = this.first.next;
                    this.first.previous = null;
                    this.size--;
                }
            }
            current = current.next;
        }

        boolean flag = false;
        for (Object o : c) {
            if (o.equals(current.value)) {
                flag = true;
            }

            if(!flag){
                this.last = this.last.previous;
                this.last.next = null;
                break;
            }
        }

        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        Link<E> current = this.first;
        while (current.next != null) {
            boolean flag = false;
            for (Object o : c) {
                if (o.equals(current.value)) flag = true;
            }
            if (flag){
                if (current.previous != null) {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                    this.size--;
                    current = current.next;
                }
                else {
                    if(this.first == this.last) {
                        this.first = null;
                        this.last = null;
                        return true;
                    }
                    else {
                        this.first = this.first.next;
                        this.first.previous = null;
                        this.size--;
                        current = this.first;
                    }

                }
            }

            this.display();
        }

        if( this.last != null) {
            boolean flag = false;
            for (Object o : c) {
                if (o.equals(current.value)) {
                    flag = true;
                }
            }
            if (flag) {
                if (this.last.previous != null) {
                    this.last = this.last.previous;
                    this.last.next = null;
                } else {
                    this.first = null;
                    this.last = null;
                }
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        for(Object e : c){
            boolean flag = false;
            for(int i = 0; i < this.size; i++){
                if (this.get(i).equals(e)) flag = true;
            }
            if(!flag) return false;
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {

        if(this.size > a.length) return a;

        for(int i = 0; i < this.size; i++){
            a[i] = this.get(i);
        }
        return a;
    }
}