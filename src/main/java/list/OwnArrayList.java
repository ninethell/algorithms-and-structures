package list;

import set.Displayable;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class OwnArrayList<E> implements List<E>, Displayable {
    private int capacity;
    private Object[] array;

    public OwnArrayList(int max){
        this.capacity = -1;
        this.array = new Object[max];
    }
    @Override
    public void display() {
        for(int i = 0; i <= this.capacity; i++) System.out.print(this.array[i] + ", ");
    }

    @Override
    public int size() {
        return this.capacity;
    }

    @Override
    public boolean isEmpty() {
        return this.capacity <= -1;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i <= this.capacity; i++){
            if (this.array[i].equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int index=0;

            @Override
            public boolean hasNext() {
                return index<=capacity;
            }

            @Override
            public E next() {
                return (E)array[index++];
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object [] a = new Object [this.capacity+1];
        System.arraycopy(this.array, 0, a, 0, this.capacity);
        return a;
    }

    @Override
    public boolean add(E o) {
        this.capacity++;
        this.array[this.capacity] = o;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i <= this.capacity; i++){
            if (this.array[i].equals(o)){
                System.arraycopy(this.array, i, this.array, i-1, this.capacity - i);
                this.capacity--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        for(E o : c){
            this.capacity++;
            this.array[this.capacity] = o;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        this.capacity += c.toArray().length;
        System.arraycopy(this.array, index, this.array, index+c.toArray().length, c.toArray().length);
        System.arraycopy(c.toArray(), 0, this.array, index, c.toArray().length);
        return true;
    }

    @Override
    public void clear() {
        this.capacity = -1;
    }

    @Override
    public E get(int index) {
        return (E)this.array[index];
    }

    @Override
    public E set(int index, Object element) {
        E a = (E)this.array[index];
        this.array[index] = (E)element;
        return a;
    }

    @Override
    public void add(int index, Object element) {
        this.capacity++;
        System.arraycopy(this.array, index, this.array, index+1, this.capacity-index);
    }

    @Override
    public E remove(int index) {
        E a = (E)this.array[index];
        System.arraycopy(this.array, index+1, this.array, index, this.capacity-index);
        this.capacity--;
        return a;
    }

    @Override
    public int indexOf(Object o) {
        for(int i = 0; i < this.capacity; i++){
            if(this.array[i].equals(o)) return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for(int i = this.capacity; i >= 0; i--){
            if(this.array[i].equals(o)) return i;
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIterator<E>() {

            int index = 0;

            int last = -1;

            @Override
            public boolean hasNext() {
                return index <= capacity;
            }

            @Override
            public E next() {
                return (E)array[index++];
            }

            @Override
            public boolean hasPrevious() {
                return index != 0;
            }

            @Override
            public E previous() {
                return (E)array[index--];
            }

            @Override
            public int nextIndex() {
                if(index == capacity) return capacity;
                return index+1;
            }

            @Override
            public int previousIndex() {
                return index-1;
            }

            @Override
            public void remove() {

            }

            @Override
            public void set(E e) {

            }

            @Override
            public void add(E e) {

            }
        };
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        List<E> a  = new OwnArrayList<E>(100);
        for(int i = fromIndex; i < toIndex; i++){
            a.add((E)this.array[i]);
        }
        return a;
    }

    @Override
    public boolean retainAll(Collection c) {
        for(int i = 0; i <= this.capacity; i++){
            boolean flag = false;
            for(Object o : c){
                if(this.array[i].equals(o)) flag = true;
            }

            if(!flag){
                System.arraycopy(this.array, i+1, this.array, i, this.capacity-i);
                this.capacity--;
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        for(int i = 0; i <= this.capacity; i++){
            boolean flag = false;
            for(Object o : c){
                if(this.array[i].equals(o)) flag = true;
            }

            if(flag){
                System.arraycopy(this.array, i+1, this.array, i, this.capacity-i);
                this.capacity--;
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean flag = false;
        for(Object o : c) {
            for (int i = 0; i <= this.capacity; i++) {
                if(this.array[i].equals(o)) flag = true;
            }
            if (!flag) return false;
        }
        return true;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length >= this.capacity){
            int i = 0;
            for (Object o : this.array){
                a[i++] = (T)o;
            }
            return a;
        }
        else{
            return (T[])toArray();
        }
    }
}