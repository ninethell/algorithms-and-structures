package list;

import java.util.ArrayList;
import java.util.Arrays;

public class OwnLinkedListTester {
    public static void main(String[] args) {
        OwnLinkedList ownLinkedList = new OwnLinkedList();

        ownLinkedList.add(0);
        ownLinkedList.add(1);
        ownLinkedList.add(2);
        ownLinkedList.add(3);

        ownLinkedList.display();

        ownLinkedList.remove(2);
        ownLinkedList.display();

        ArrayList<Integer> c = new ArrayList<>(Arrays.asList(10, 11, 12));

        ownLinkedList.addAll(c);
        ownLinkedList.display();

//ownLinkedList.retainAll(c);
        ownLinkedList.display();

        //ownLinkedList.removeAll(c);
        ownLinkedList.display();

        System.out.println(ownLinkedList.size());
        System.out.println(ownLinkedList.get(2));
        System.out.println(ownLinkedList.containsAll(c));

    }
}