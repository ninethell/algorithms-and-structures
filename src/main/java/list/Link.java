package list;

public class Link<E> {

    public E value;
    public Link<E> next;
    public Link<E> previous;

    public Link(E value) {
        this.value = value;
        this.next = null;
        this.previous = null;
    }

    public boolean hasNext(){
        return next != null;
    }
    public boolean hasPrevious(){
        return previous != null;
    }

}