package merge;

public class Main {
    public static void main(String[] args) {
        int max=100;
        Merge merge =new Merge(max);

        merge.insert(64);
        merge.insert(21);
        merge.insert(33);
        merge.insert(70);
        merge.insert(12);
        merge.insert(85);
        merge.insert(44);
        merge.insert(3);
        merge.insert(99);
        merge.insert(0);
        merge.insert(108);
        merge.insert(36);

        merge.display();
        merge.mergeSort();
        merge.display();

    }
}