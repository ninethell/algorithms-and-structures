package classWork;
import java.util.Optional;

public class StudentInGroup implements Comparable<StudentInGroup> {

    private Optional<String> groupNumber;
    private String name;
    private boolean isBoy;
    private int age;

    public StudentInGroup(String groupNumber, String name, boolean isBoy, int age) {
        this.groupNumber = Optional.ofNullable(groupNumber);
        this.name = name;
        this.isBoy = isBoy;
        this.age = age;
    }

    @Override
    public int compareTo(StudentInGroup o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "StudentInGroup{" +
                "groupNumber=" + groupNumber +
                ",\n name='" + name + '\'' +
                ",\n isBoy=" + isBoy +
                ",\n age=" + age +
                '}';
    }

    public Optional<String> getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = Optional.ofNullable(groupNumber);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBoy() {
        return isBoy;
    }

    public void setBoy(boolean boy) {
        isBoy = boy;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        return this.isBoy == ((StudentInGroup) obj).isBoy
                && this.age == ((StudentInGroup) obj).age
                && this.name.equals(((StudentInGroup) obj).name)
                && this.groupNumber.equals(((StudentInGroup) obj).groupNumber);
    }
}