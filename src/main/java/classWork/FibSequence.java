package classWork;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FibSequence {
    public static void main(String[] args) {
        System.out.println(getFibSequence(1));
        System.out.println(fibMultiplication(1));
    }

    public static List<Integer> getFibSequence(int n) {
        return Stream.iterate(new int[]{1, 1}, ints -> new int[]{ints[1], ints[0] + ints[1]}).limit(n).map(ints -> ints[0]).collect(Collectors.toList());
    }

    public static long fibMultiplication(int n) {
        Optional<Integer> result = getFibSequence(n).stream().reduce((x, y) -> x * y);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new IllegalArgumentException("Не выходит: " + n);
        }
    }
}
