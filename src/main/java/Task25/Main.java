package Task25;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    static class FibCallable implements Callable<Map<Integer, Long>> {
        @Override
        public Map<Integer, Long> call() {
            HashMap<Integer, Long> num = new HashMap<>();
            long before = System.nanoTime();
            int fib = fib(10);
            long after = System.nanoTime();
            long time = after - before;
            num.put(fib, time);
            System.out.println("Finished in thread: " + Thread.currentThread().getName());
            return num;
        }

        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                return fib(n - 2) + fib(n - 1);
            } else {
                return 1;
            }
        }
    }

    public static void main(String[] args) throws Exception {

        ArrayList<FibCallable> threads = new ArrayList<>();
        for(int i = 0; i < 10; i++) threads.add(new FibCallable());

        ExecutorService s1 = Executors.newCachedThreadPool();
        long before = System.nanoTime();
        for(int i = 0; i < 10; i++) s1.submit(threads.get(i));
        s1.shutdown();
        s1.awaitTermination(100, TimeUnit.MINUTES);
        long after = System.nanoTime();
        System.out.println("Cached thread pool: " + (after - before));

        ExecutorService s2 = Executors.newFixedThreadPool(10);
        before = System.nanoTime();
        for(int i = 0; i < 10; i++) s2.submit(threads.get(i));
        s2.shutdown();
        s2.awaitTermination(100, TimeUnit.MINUTES);
        after = System.nanoTime();
        System.out.println("Fixed thread pool: " + (after - before));

        ExecutorService s3 = Executors.newSingleThreadExecutor();
        before = System.nanoTime();
        for(int i = 0; i < 10; i++) s3.submit(threads.get(i));
        s3.shutdown();
        s3.awaitTermination(100, TimeUnit.MINUTES);
        after = System.nanoTime();
        System.out.println("Single thread pool: " + (after - before));
    }
}
