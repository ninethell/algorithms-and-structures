package task26;

public class Third implements Runnable, Changeable {
    volatile boolean flag = true;

    @Override
    public boolean getFlag() {
        return false;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10);
            flag = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}