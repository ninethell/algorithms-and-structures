package task26;

import java.util.ArrayList;

public class Main {
    static ArrayList<Long> firstCycleTimes = new ArrayList<>();
    static ArrayList<Long> secondCycleTimes = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            Thread thread = new Thread(new First());

            thread.start();
            thread.join();
        }

        System.out.println("First statistics: " + firstCycleTimes.stream().mapToLong(e -> e).summaryStatistics());
        System.out.println("Second statistics: " + secondCycleTimes.stream().mapToLong(e -> e).summaryStatistics());
    }
}