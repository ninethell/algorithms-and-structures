package task26;

public class Second implements Runnable, Changeable {
    boolean flag = true;

    @Override
    public boolean getFlag() {
        return flag;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10);
            flag = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
