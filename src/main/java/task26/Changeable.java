package task26;

public interface Changeable {
    boolean getFlag();
}
