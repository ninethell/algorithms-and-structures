package task26;

public class First implements Runnable {
    Changeable second = new Second();
    Changeable third = new Third();
    long firstCycleTime;
    long secondCycleTime;

    @Override
    public void run() {

        new Thread(((Runnable) second)).start();

        long before = System.nanoTime();

        while (second.getFlag()) {
            Thread.yield();
        }

        long after = System.nanoTime();
        firstCycleTime = after - before;

        new Thread(((Runnable) third)).start();

        before = System.nanoTime();
        while (third.getFlag()) {
            Thread.yield();
        }

        after = System.nanoTime();
        secondCycleTime = after-before;

        Main.firstCycleTimes.add(firstCycleTime);
        Main.secondCycleTimes.add(secondCycleTime);
    }
}