package arrayContainer;

public class SortedArrayContainer {

    private long[] array;
    private int capacity;

    public SortedArrayContainer(int max) {
        this.array = new long[max];
        this.capacity = -1;
    }


    public long readUnit(int index) {
        if (index <= this.capacity) {
            return this.array[index];
        } else {
            throw new IndexOutOfBoundsException("Our internal capacity index is smaller than requested in read unit operation");
        }
    }

    private void insert(int index, long value){
        this.capacity++;
        for (int i = this.capacity; i > index; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[index] = value;
    }

    public void remove(int index) {
        if (index <= this.capacity) {
            for (int i = index; i < capacity; i++) {
                this.array[i] = this.array[i + 1];
            }
            this.array[capacity--] = 0;
        } else {
            throw new IndexOutOfBoundsException("Our internal capacity index is smaller than requested in remove operation");
        }
    }

    public void oldInsert(long value) {
        int i = 1;
        boolean flag = false;
            if (value < this.array[0]) {
                insert(0, value);
                flag = true;
            }

            if (value > this.array[this.capacity]) {
                insert(this.capacity + 1, value);
                flag = true;
            }

            if(!flag) {
                while (i < this.capacity) {
                    if ((this.array[i-1] < value) && (value < this.array[i])) {
                        insert(i, value);
                        break;
                    }
                    i++;
                }
            }
    }

    public void newInsert(long value) {
            int L = 0;
            int R = this.capacity;

            while (L != R)
            {
                if(value < this.array[0]){
                    insert(0, value);
                    break;
                }

                if(value > this.array[this.capacity]){
                    insert(this.capacity+1, value);
                    break;
                }

                int mid = L + (R - L) / 2;
                if (array[mid] > value) {
                    if (array[mid-1] < value) {
                        insert(mid, value);
                        break;
                    }
                    else R = mid;
                }
                if (array[mid] < value){
                    if (array[mid+1] > value){
                        insert(mid+1, value);
                        break;
                    }
                    else L = mid;
                }
            }
    }

    public void add(long value){

        this.capacity++;
        this.array[this.capacity] = value;

    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= this.capacity; i++) {
            stringBuilder.append(this.array[i]);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

}