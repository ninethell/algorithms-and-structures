package arrayContainer;


public class ArrayTimeTester {
    public static void main(String[] args) {
        SortedArrayContainer hundredUnitsOld = new SortedArrayContainer(100);
        SortedArrayContainer hundredUnitsNew = new SortedArrayContainer(100);
        SortedArrayContainer thousandUnitsOld = new SortedArrayContainer(1000);
        SortedArrayContainer thousandUnitsNew = new SortedArrayContainer(1000);
        SortedArrayContainer tenThousandsUnitsOld = new SortedArrayContainer(10000);
        SortedArrayContainer tenThousandsUnitsNew = new SortedArrayContainer(10000);


        int ran1 = (int)(Math.random()*100);
        for (int i = 0; i < ran1; i++){
            hundredUnitsOld.add(i);
            hundredUnitsNew.add(i);
        }
        for (int i = ran1; i < 99; i++) {
            hundredUnitsOld.add(i+1);
            hundredUnitsNew.add(i+1);
        }

        int ran2 = ran1*10;
        for (int i = 0; i < ran2; i++) {
            thousandUnitsOld.add(i);
            thousandUnitsNew.add(i);
        }
        for (int i = ran2; i < 999; i++) {
            thousandUnitsOld.add(i+1);
            thousandUnitsNew.add(i+1);
        }

        int ran3 = ran2*10;
        for (int i = 0; i < ran3; i++) {
            tenThousandsUnitsOld.add(i);
            tenThousandsUnitsNew.add(i);
        }
        for (int i = ran3; i < 9999; i++) {
            tenThousandsUnitsOld.add(i+1);
            tenThousandsUnitsNew.add(i+1);
        }

        long [][] results = new long [2][3];

        long beginning = System.nanoTime();
        hundredUnitsOld.oldInsert(ran1);
        long end = System.nanoTime();
        results[0][0] = end - beginning;

        beginning = System.nanoTime();
        hundredUnitsNew.newInsert(ran1);
        end = System.nanoTime();
        results[1][0] = end - beginning;

        beginning = System.nanoTime();
        thousandUnitsOld.oldInsert(ran2);
        end = System.nanoTime();
        results[0][1] = end - beginning;

        beginning = System.nanoTime();
        thousandUnitsNew.newInsert(ran2);
        end = System.nanoTime();
        results[1][1] = end - beginning;

        beginning = System.nanoTime();
        tenThousandsUnitsOld.oldInsert(ran3);
        end = System.nanoTime();
        results[0][2] = end - beginning;


        beginning = System.nanoTime();
        tenThousandsUnitsNew.newInsert(ran3);
        end = System.nanoTime();
        results[1][2] = end - beginning;

        System.out.println("   " + " " +  "100" + " " +  "1000" + " " +  "10000");
        System.out.println("Old" + " " +  results[0][0] + " " + results[0][1] + " " +  results[0][2]);
        System.out.println("New" + " " +  results[1][0] + " " + results[1][1] + " " +  results[1][2]);

    }
}
