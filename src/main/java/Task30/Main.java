package Task30;

public class Main {
    public static void main(String[] args) {
        DelayedTaskProducer producer = new DelayedTaskProducer();
        new Thread(producer).start();
        new Thread(new DelayedTaskConsumer(producer)).start();
    }
}
