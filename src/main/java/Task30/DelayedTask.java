package Task30;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayedTask implements Runnable, Delayed {
    private long startTime;
    private Procedure task;

    public DelayedTask(long delay, Procedure task) {
        this.startTime = System.currentTimeMillis() + delay;
        this.task = task;
    }

    @Override
    public int compareTo(Delayed o) {
        return Math.toIntExact(this.startTime - ((DelayedTask) o).startTime);
    }

    @Override
    public void run() {
        task.invoke();
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(startTime - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }
}