package Task30;

@FunctionalInterface
public interface Procedure {
    void invoke();
}
