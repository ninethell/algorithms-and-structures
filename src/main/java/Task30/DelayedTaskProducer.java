package Task30;

import java.util.Random;
import java.util.concurrent.DelayQueue;

public class DelayedTaskProducer implements Runnable {
    private Random random = new Random();
    DelayQueue<DelayedTask> queue = new DelayQueue<>();

    public synchronized DelayedTask getTask() throws InterruptedException {
        return queue.take();
    }

    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            int delay = random.nextInt(1000);
            DelayedTask task = new DelayedTask(delay, () -> System.out.println("Doing random task after " + delay + " ms of waiting"));
            queue.add(task);
            try {
                Thread.sleep(5 * 1000 * 60 / 50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        queue.add(new DelayedTask(0, () -> System.exit(0)));

    }
}
