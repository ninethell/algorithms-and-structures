package Task30;

public class DelayedTaskConsumer implements Runnable {

    private DelayedTaskProducer producer;

    public DelayedTaskConsumer(DelayedTaskProducer producer) {
        this.producer = producer;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("\nTrying to get task");
            try {
                System.out.println("Taken task from consumer and started");
                new Thread(producer.getTask()).start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}