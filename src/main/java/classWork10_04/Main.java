package classWork10_04;

import classWork.StudentInGroup;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        StudentInGroup student1 = new StudentInGroup(null, "Артем", true, 18);
        StudentInGroup student2 = new StudentInGroup("11-805", "Эмиль", true, 20);
        StudentInGroup student3 = new StudentInGroup("11-801", "Сюмбель", false, 21);
        StudentInGroup student4 = new StudentInGroup(null, "Аня", false, 17);

        ArrayList<StudentInGroup> students = new ArrayList<>();

        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);

        addStudentListToFile("students", students);
        List<StudentInGroup> deserialized = readStudentsFromFile("students");

        System.out.println(deserialized);
    }

    private static void addStudentListToFile(String filename, List<StudentInGroup> students) throws IOException {
        FileOutputStream fos = new FileOutputStream(filename);
        DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));

        for (StudentInGroup student : students) {
            System.out.println(student);
            outStream.writeUTF(student.getName());
            outStream.writeInt(student.getAge());
            outStream.writeBoolean(student.isBoy());
            outStream.writeUTF(student.getGroupNumber().orElse("NULLPTR"));
        }

        outStream.close();
    }

    private static List<StudentInGroup> readStudentsFromFile(String filename) throws IOException {
        FileInputStream fis = new FileInputStream(filename);
        DataInputStream reader = new DataInputStream(fis);
        ArrayList<StudentInGroup> arrayList = new ArrayList<>();
        while (reader.available() > 0) {
            String name = reader.readUTF();
            int age = reader.readInt();
            boolean gender = reader.readBoolean();
            String group = reader.readUTF();
            group = group.equals("NULLPTR") ? null : group;


            arrayList.add(new StudentInGroup(group, name, gender, age));
        }
        return arrayList;
    }
}
