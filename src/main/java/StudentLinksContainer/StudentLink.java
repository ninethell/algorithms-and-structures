package StudentLinksContainer;

public class StudentLink {

    public StudentLinked value;
    public StudentLink next;
    public StudentLink previous;

    public StudentLink(StudentLinked value){
        this.value = value;
    }
    //когда надо вставить в начало двухсвязного списка
    public StudentLink(StudentLinked value, StudentLink next) {
        this.value = value;
        this.previous = null;
        this.next = next;
    }

    //когда надо вставить в какое-то место двухсвязного списка
    public StudentLink(StudentLinked value, StudentLink next, StudentLink previous) {
        this.value = value;
        this.previous = previous;
        this.next = next;
    }

    public boolean hasNext() {
        return next != null;
    }

    /*public void setLink(Link link) {
        this.link = link;
    }

    public Link getLink() {
        return link;
    }*/
}
