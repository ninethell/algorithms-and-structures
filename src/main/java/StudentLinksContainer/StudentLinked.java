package StudentLinksContainer;

import java.util.Calendar;
import java.util.Date;

public class StudentLinked {

    private String firstName;
    private String lastName;
    private String patronymic;
    private Date age;

    public StudentLinked(String firstName, String lastName, String patronymic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getAge() {
        return age;
    }

    public void setAge(Date age) {
        this.age = age;
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Фамилия: ");
        stringBuilder.append(this.lastName);
        stringBuilder.append("\nИмя: ");
        stringBuilder.append(this.firstName);
        stringBuilder.append("\nОтчество: ");
        stringBuilder.append(this.patronymic);
        stringBuilder.append("\nВозраст: ");

        Calendar dateOfBirth = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dateOfBirth.setTime(this.age);
        dateOfBirth.add(Calendar.DAY_OF_MONTH, -1);
        int yearsOld = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dateOfBirth.get(Calendar.DAY_OF_YEAR)) yearsOld--;

        stringBuilder.append(yearsOld);

        stringBuilder.append("\n");
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }
}