package StudentLinksContainer;

import studentArrayContainer.Student;

import java.util.Calendar;
import java.util.Date;

public class StudentLinksContainer {

    private StudentLink first;
    private StudentLink last;

    public void add(StudentLinked value) {
        StudentLink studentLink = new StudentLink(value, first);
        studentLink.next = first;
        if (first != null) first.previous = studentLink;
        this.first = studentLink;
        if (this.last == null) this.last = studentLink;
    }

    public void insert(StudentLinked value) {
        boolean isAlone = false;
        if (this.first == null){
            StudentLink mid = new StudentLink(value);
            isAlone = true;
            this.first = mid;
            this.last = mid;
        }
        if ((this.first.next == null) && (this.first.previous == null)) {
            if (value.getAge().getTime() > first.value.getAge().getTime()) {
                System.out.println(first.value);
                System.out.println(last.value);
                StudentLink mid = new StudentLink(value, first);
                first.previous = mid;
                isAlone = true;
                this.first = mid;
            }
            if (value.getAge().getTime() < first.value.getAge().getTime()) {
                StudentLink mid = new StudentLink(value, null, first);
                first.next = mid;
                isAlone = true;
                this.last = mid;
            }
        }


        if (!isAlone){
            StudentLink left = first;
            StudentLink right = first.next;
            while (true) {
                if (value.getAge().getTime() < left.value.getAge().getTime()) {
                    if (right != null) {
                        if (value.getAge().getTime() > right.value.getAge().getTime()) {
                            StudentLink mid = new StudentLink(value, right, left);
                            left.next = mid;
                            right.previous = mid;
                            break;
                        }
                        left = right;
                        right = right.next;
                    } else {
                        StudentLink mid = new StudentLink(value);
                        mid.previous = left;
                        left.next = mid;
                        this.last = mid;
                        break;
                    }
                }
            }
        }
    }


    //по увеличеснию возраста
    public String descending() {
        StringBuilder stringBuilder = new StringBuilder();
        StudentLink current = this.first;
        if (current != null) {
            stringBuilder.append(current.value.toString());
            stringBuilder.append(" ");
            while (current.next != null) {
                stringBuilder.append(current.next.value.toString());
                stringBuilder.append(" ");
                current = current.next;
            }
        }
        return stringBuilder.toString();
    }

    //по уменьшению возраста
    public String ascending() {
        StringBuilder stringBuilder = new StringBuilder();
        StudentLink current = this.last;
        if (current != null) {
            stringBuilder.append(current.value.toString());
            stringBuilder.append(" ");
            while (current.previous != null) {
                stringBuilder.append(current.previous.value.toString());
                stringBuilder.append(" ");
                current = current.previous;
            }
        }
        return stringBuilder.toString();
    }


    public String getUnitAge(String firstName, String lastName, String patronymic) {
        StudentLink current = this.first;

        while (true) {
            if ((current.value.getFirstName().equals(firstName)) && (current.value.getLastName().equals(lastName)) && (current.value.getPatronymic().equals(patronymic))) {
                Calendar dateOfBirth = Calendar.getInstance();
                Calendar today = Calendar.getInstance();

                dateOfBirth.setTime(current.value.getAge());
                dateOfBirth.add(Calendar.DAY_OF_MONTH, -1);
                int yearsOld = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
                if (today.get(Calendar.DAY_OF_YEAR) <= dateOfBirth.get(Calendar.DAY_OF_YEAR)) yearsOld--;
                String s = "";
                s+=yearsOld;
                return s;
            }
            if (current.next != null) current = current.next;
            else return "Нет такого элемента, дурачок!!!!";
        }

    }



    public void remove(String firstName, String lastName, String patronymic){
        StudentLink current = this.first;

        while(true) {
            if (current != null) {
                if ((current.value.getFirstName().equals(firstName)) && (current.value.getLastName().equals(lastName)) && (current.value.getPatronymic().equals(patronymic))){
                    if(current.previous != null) {
                        current.previous.next = current.next;
                        current.next.previous = current.previous;
                    }
                    else {
                        current.next.previous = null;
                        this.first = current.next;
                    }
                    break;
                }
            }
            if (current != null) current = current.next;
            else {
                System.out.println("Такого элемента нет, дурачок!!!");
                break;
            }
        }
    }

}
