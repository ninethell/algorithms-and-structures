package StudentLinksContainer;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StudentTester {
    public static void main(String[] args) throws ParseException {

        StudentLinksContainer studentLinksContainer = new StudentLinksContainer();

        StudentLinked studentLinked1 = new StudentLinked("a1", "b1", "c1");
        StudentLinked studentLinked2 = new StudentLinked("a2", "b2", "c2");
        StudentLinked studentLinked3 = new StudentLinked("a3", "b3", "c3");
        StudentLinked studentLinked4 = new StudentLinked("a4", "b4", "c4");

        studentLinked1.setAge(new SimpleDateFormat("ddmmyyyy").parse("25122000"));//gettime 5
        studentLinked2.setAge(new SimpleDateFormat("ddmmyyyy").parse("13111999"));//gettime 4
        studentLinked3.setAge(new SimpleDateFormat("ddmmyyyy").parse("08011998"));//gettime 3
        studentLinked4.setAge(new SimpleDateFormat("ddmmyyyy").parse("20011998"));//gettime 2

        StudentLinked studentLinked5 = new StudentLinked("a5", "b5", "c5");
        studentLinked5.setAge(new SimpleDateFormat("ddmmyyyy").parse("24121997"));//gettime 4.5

        studentLinksContainer.insert(studentLinked1);
        studentLinksContainer.insert(studentLinked2);
        studentLinksContainer.insert(studentLinked3);
        studentLinksContainer.insert(studentLinked4);
        studentLinksContainer.insert(studentLinked5);
        System.out.println(studentLinksContainer.ascending());
        System.out.println("по-другому");
        System.out.println(studentLinksContainer.descending());

        studentLinksContainer.remove("a2", "b2", "c2");
        studentLinksContainer.remove("a3", "b3", "c3");

        System.out.println(studentLinksContainer.getUnitAge("a1", "b1", "c1"));
        System.out.println();

        System.out.println(studentLinksContainer.descending());

    }
}
