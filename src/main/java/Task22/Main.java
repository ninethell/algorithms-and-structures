package Task22;

import java.util.ArrayList;

public class Main {
    static class Fib implements Runnable {
        @Override
        public void run() {
            long start = System.nanoTime();
            fib(13);
            long time = (System.nanoTime() - start) / 1000000;
            System.out.println(Thread.currentThread().getName() + " вычислил за " + time + " миллисек.");
        }

        private int fib(int n) {
            if ((n != 0) && (n != 1)) {
                int num = fib(n - 2) + fib(n - 1);
                System.out.println(Thread.currentThread().getName() + ": " + num);
                return num;
            } else {
                System.out.println(Thread.currentThread().getName() + ": " + 1);
                return 1;
            }
        }
    }

    public static void main(String[] args) {
        ArrayList<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 7; i++) threads.add(new Thread(new Fib()));

        for (int i = 0; i < 7; i++) {
            threads.get(i).setPriority(i + 1);
            threads.get(i).setName("Приоритет " + (i + 1));
        }

        for (int i = 0; i < 7; i++) threads.get(i).start();
    }
}
