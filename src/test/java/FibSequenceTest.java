import classWork.FibSequence;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FibSequenceTest {
    @Test
    public void generationTest() {
        List<Integer> list0 = Arrays.asList();
        List<Integer> list1 = Arrays.asList(1);
        List<Integer> list5 = Arrays.asList(1, 1, 2, 3, 5);
        List<Integer> list10 = Arrays.asList(1, 1, 2, 3, 5, 8, 13, 21, 34, 55);

        assertEquals(FibSequence.getFibSequence(0), list0);
        assertEquals(FibSequence.getFibSequence(1), list1);
        assertEquals(FibSequence.getFibSequence(5), list5);
        assertEquals(FibSequence.getFibSequence(10), list10);
    }

    @Test
    public void multiplicationTest() {
        long mult1 = 1;
        long mult5 = 30;
        long mult10 = 122522400;
        boolean exceptionThrownAtZero = false;

        try {
            FibSequence.fibMultiplication(0);
        } catch (IllegalArgumentException e) {
            exceptionThrownAtZero = true;
        }

        assertTrue(exceptionThrownAtZero);
        assertEquals(FibSequence.fibMultiplication(1), mult1);
        assertEquals(FibSequence.fibMultiplication(5), mult5);
        assertEquals(FibSequence.fibMultiplication(10), mult10);
    }
}
